from enum import Enum


class Color(Enum):
    GREEN = 0
    BLACK = 1
    WHITE = 2

