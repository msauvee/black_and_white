from typing import List

from Color import Color
from Pawn import Pawn
from Position import Position


class Direction:

    def __init__(self, column_increment, row_increment):
        self._column_increment = column_increment
        self._row_increment = row_increment

    @property
    def column_increment(self):
        return self._column_increment

    @property
    def row_increment(self):
        return self._row_increment

    @staticmethod
    def get_directions():
        directions = []
        for _column_increment in range(-1, 2):
            for _row_increment in range(-1, 2):
                if not (_column_increment == 0 and _row_increment == 0):
                    directions.append(Direction(_column_increment, _row_increment))
        return directions


ALL_DIRECTIONS = Direction.get_directions()


class Game:
    _size: int

    def __init__(self, cells):
        size = len(cells)
        if size < 6:
            raise ValueError("Board size can't be lower than 6")
        if size % 2 != 0:
            raise ValueError("Board size can't be odd")
        self._size = size
        self._cells = [None] * self._size
        for row in range(0, size):
            if len(cells[row]) != self._size:
                raise ValueError(f"Board has only {len(cells[row])} items for row {row}, but {self._size} columns")
            self._cells[row] = [Color.GREEN] * self._size
            for column in range(0, size):
                self._cells[row][column] = Color(cells[row][column])

    def __is_inside(self, position):
        return 0 <= position.column < self._size \
               and 0 <= position.row < self._size

    def __flip(self, position):
        pawn = self.get_pawn(position)
        if pawn is None:
            raise ValueError(f"can't flip pawn as there is no pawn in position {position.display_name}")
        self.__set_color(pawn.flip(), position)

    def __is_valid_turn(self, color, position):
        return len(self.__get_all_flippable_pawns(color, position)) > 0

    def __get_direct_pawns(self, color, position):
        direct_pawns: List[Direction] = []
        for direction in ALL_DIRECTIONS:
            direct_position = position.add(direction)
            if not self.__is_inside(direct_position):
                continue
            pawn = self.get_pawn(direct_position)
            if pawn is not None and pawn.color != color:
                direct_pawns.append(direction)
        return direct_pawns

    def __get_all_flippable_pawns_for_one_direction(self, color, position, direction):
        """
        Return the list of opponent's pawns that can be flipped.
        :position: starting position. The pawn at this position is not taken into account.
        """
        flip_positions_in_one_direction = []
        # go at the first pawn in the direction
        flip_position = position.add(direction)
        while True:
            # if we get outside of game or no more pawn, then nothing to flip
            if not self.__is_inside(flip_position) or self.get_pawn(flip_position) is None:
                flip_positions_in_one_direction.clear()
                break
            # if this is our color, just stop
            if self.get_pawn(flip_position).color == color:
                break
            # still opponent pawn, flip
            flip_positions_in_one_direction.append(flip_position)
            # and continue next one
            flip_position = flip_position.add(direction)

        return flip_positions_in_one_direction

    def __get_all_flippable_pawns(self, color, position):
        # not valid if the cell is not empty
        if not self.get_pawn(position) is None:
            return []
        # check there is at least opponent pawn around
        direct_pawns = self.__get_direct_pawns(color, position)
        if len(direct_pawns) == 0:
            return []
        # check that it will flip something
        flip_positions = []
        for direction in direct_pawns:
            flip_positions_in_one_direction = \
                self.__get_all_flippable_pawns_for_one_direction(color, position, direction)
            if len(flip_positions_in_one_direction) > 0:
                flip_positions.extend(flip_positions_in_one_direction)
        return flip_positions

    def __get_color(self, position):
        return self._cells[position.row][position.column]

    def __set_color(self, color, position):
        self._cells[position.row][position.column] = color

    def get_pawn(self, position):
        if not self.__is_inside(position):
            raise ValueError("can't get a value out of game")
        color = self.__get_color(position)
        if color == Color.GREEN:
            return None
        return Pawn(color, position)

    def is_valid_move(self, color, position):
        """
        Play a turn for a player at a position.
        :param color: color of the player
        :param position: position of the pawn
        :return: True if the turn is valid, False otherwise
        """
        flip_positions = self.__get_all_flippable_pawns(color, position)
        # at least one position to be valid
        if len(flip_positions) == 0:
            return False
        # set the color on the position
        self.__set_color(color, position)
        # do the flip
        for position in flip_positions:
            self.__flip(position)
        return True

    def get_valid_moves(self, color):
        valid_turns = []
        for column in range(0, self._size):
            for row in range(0, self._size):
                position = Position(column, row)
                if self.__is_valid_turn(color, position):
                    valid_turns.append(position)
        return valid_turns

    def is_player_blocked(self, color):
        return len(self.get_valid_moves(color)) == 0

    def get_scores(self):
        """ Compute scores. The function return black score and then white score."""
        white_score = black_score = 0;
        for column in range(0, self._size):
            for row in range(0, self._size):
                if self.__get_color(Position(column, row)) == Color.WHITE:
                    white_score += 1
                elif self.__get_color(Position(column, row)) == Color.BLACK:
                    black_score += 1
        return black_score, white_score
