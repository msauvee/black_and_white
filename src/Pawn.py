from Color import Color
from Position import Position


class Pawn:
    _color: int
    _position: Position

    @property
    def color(self):
        return self._color

    @property
    def position(self):
        return self._position

    def __init__(self, color, position):
        if color != Color.WHITE and color != Color.BLACK:
            raise ValueError("Pawn can't have color {}".format(color.name))
        self._color = color
        self._position = position

    def is_opponent(self, pawn):
        if pawn is None:
            return False
        return self._color != pawn.color

    def flip(self):
        self._color = Color.WHITE if self._color == Color.BLACK else Color.BLACK
        return self._color
