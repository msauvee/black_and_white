class Position:

    def __init__(self, column, row):
        self._column = column
        self._row = row

    def add(self, direction):
        return Position(self.column + direction.column_increment, self.row + direction.row_increment)

    @property
    def column(self):
        return self._column

    @property
    def row(self):
        return self._row

    def display_name(self):
        return self.column + "," + self.row
