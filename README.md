#Othello Reversi game

## How to
 
### How to create a start game ?
Default size is 8. You have to provide a 2 dimension array like this:

```
    board = [
        [0, 0, 0, 0, 0, 0, 0, 0],
        [0, 0, 0, 0, 0, 0, 0, 0],
        [0, 0, 0, 0, 0, 0, 0, 0],
        [0, 0, 0, 2, 1, 0, 0, 0],
        [0, 0, 0, 1, 2, 0, 0, 0],
        [0, 0, 0, 0, 0, 0, 0, 0],
        [0, 0, 0, 0, 0, 0, 0, 0],
        [0, 0, 0, 0, 0, 0, 0, 0]
    ]
    game = Game(board)
```

### How to create a different size boarder ?
Provide a different size array. The minimum size is 6, max 27. All rows should have the same number of columns. The number of columns (or row) should be odd.

```
    board = [
        [0, 0, 0, 0, 0, 0],
        [0, 0, 0, 0, 0, 0],
        [0, 0, 2, 1, 0, 0],
        [0, 0, 1, 2, 0, 0],
        [0, 0, 0, 0, 0, 0],
        [0, 0, 0, 0, 0, 0]
    ]
    game = Game(board)
```

### How to check if a play is not valid ?
The function is_valid_move will False if the move is invalid, True otherwise. In the following example, the turn is invalid as it will not flip any pawn.

```
    board = [
        [0, 0, 0, 0, 0, 0, 0, 0],
        [0, 0, 0, 0, 0, 0, 0, 0],
        [0, 0, 0, 0, 0, 0, 0, 0],
        [0, 0, 0, 2, 1, 0, 0, 0],
        [0, 0, 0, 1, 2, 0, 0, 0],
        [0, 0, 0, 0, 0, 0, 0, 0],
        [0, 0, 0, 0, 0, 0, 0, 0],
        [0, 0, 0, 0, 0, 0, 0, 0]
    ]
    game = Game(board)
    assert not game.is_valid_move(Color.WHITE, Position(3, 3))
``` 

### How to get the list of all valid moves ?
The function get_valid_moves of Game instance returns the list of valid moves.

```
    board = [
        [0, 0, 0, 2, 0, 0],
        [2, 1, 1, 0, 0, 0],
        [0, 1, 2, 0, 0, 0],
        [0, 1, 2, 0, 0, 0],
        [0, 0, 0, 0, 0, 0],
        [0, 0, 0, 0, 0, 0]
    ]
    game = Game(board)
    valid_turns = game.get_valid_moves(Color.WHITE)
```

### How to get the current score ?
The function get_score of a Game instance return the current score of BLACK and WHITE players.

```
    board = [
        [0, 0, 0, 2, 0, 0],
        [2, 1, 1, 0, 0, 0],
        [0, 1, 2, 0, 0, 0],
        [0, 1, 2, 0, 0, 0],
        [0, 1, 0, 0, 0, 0],
        [0, 0, 0, 0, 0, 0]
    ]
    game = Game(board)
    black_score, white_score = game.get_scores() 
```
