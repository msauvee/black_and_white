from Color import Color
from Game import Game
from Position import Position


def test_player_cant_play_no_more_valid_play():
    board = [
        [2, 2, 2, 1, 1, 1],
        [0, 0, 0, 0, 0, 0],
        [0, 0, 0, 0, 0, 0],
        [0, 0, 0, 0, 0, 0],
        [0, 0, 0, 0, 0, 0],
        [0, 0, 0, 0, 0, 0]
    ]
    game = Game(board)
    assert game.is_player_blocked(Color.WHITE)
    assert game.is_player_blocked(Color.BLACK)


def test_player_cant_play_board_full():
    board = [
        [2, 2, 2, 1, 1, 1],
        [2, 2, 2, 1, 1, 1],
        [2, 2, 2, 1, 1, 1],
        [2, 2, 2, 1, 1, 1],
        [2, 2, 2, 1, 1, 1],
        [2, 2, 2, 1, 1, 1]
    ]
    game = Game(board)
    assert game.is_player_blocked(Color.WHITE)
    assert game.is_player_blocked(Color.BLACK)
