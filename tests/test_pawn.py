import pytest

from Color import Color
from Pawn import Pawn
from Position import Position


def test_pawn_cant_be_green():
    with pytest.raises(ValueError):
        Pawn(Color.GREEN, Position(1, 1))
