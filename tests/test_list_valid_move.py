from Color import Color
from Game import Game
from Position import Position
from Pawn import Pawn


def position_within_list(position, positions):
    for item in positions:
        if item.column == position.column and item.row == position.row:
            return True
    return False


def test_list_valid_plays_from_spec_2_1_back():
    board = [
        [0, 0, 0, 0, 0, 0],
        [2, 1, 2, 0, 0, 0],
        [0, 1, 2, 0, 0, 0],
        [0, 1, 2, 0, 0, 0],
        [0, 0, 0, 0, 0, 0],
        [0, 0, 0, 0, 0, 0]
    ]
    game = Game(board)
    valid_turns = game.get_valid_moves(Color.BLACK)
    assert len(valid_turns) == 5
    assert position_within_list(Position(3, 0), valid_turns)
    assert position_within_list(Position(3, 1), valid_turns)
    assert position_within_list(Position(3, 2), valid_turns)
    assert position_within_list(Position(3, 3), valid_turns)
    assert position_within_list(Position(3, 4), valid_turns)


def test_list_valid_plays_from_spec_2_1_white():
    board = [
        [0, 0, 0, 2, 0, 0],
        [2, 1, 1, 0, 0, 0],
        [0, 1, 2, 0, 0, 0],
        [0, 1, 2, 0, 0, 0],
        [0, 0, 0, 0, 0, 0],
        [0, 0, 0, 0, 0, 0]
    ]
    game = Game(board)
    valid_turns = game.get_valid_moves(Color.WHITE)
    assert len(valid_turns) == 6
    assert position_within_list(Position(0, 0), valid_turns)
    assert position_within_list(Position(0, 2), valid_turns)
    assert position_within_list(Position(0, 3), valid_turns)
    assert position_within_list(Position(0, 4), valid_turns)
    assert position_within_list(Position(2, 0), valid_turns)
    assert position_within_list(Position(3, 1), valid_turns)
