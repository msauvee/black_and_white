from Game import Game


def test_score():
    board = [
        [0, 0, 0, 2, 0, 0],
        [2, 1, 1, 0, 0, 0],
        [0, 1, 2, 0, 0, 0],
        [0, 1, 2, 0, 0, 0],
        [0, 1, 0, 0, 0, 0],
        [0, 0, 0, 0, 0, 0]
    ]
    game = Game(board)
    black_score, white_score = game.get_scores()
    assert black_score == 5
    assert white_score == 4