import pytest

from Color import Color
from Game import Game
from Position import Position


def test_initial_state():
    board = [
        [0, 0, 0, 0, 0, 0, 0, 0],
        [0, 0, 0, 0, 0, 0, 0, 0],
        [0, 0, 0, 0, 0, 0, 0, 0],
        [0, 0, 0, 2, 1, 0, 0, 0],
        [0, 0, 0, 1, 2, 0, 0, 0],
        [0, 0, 0, 0, 0, 0, 0, 0],
        [0, 0, 0, 0, 0, 0, 0, 0],
        [0, 0, 0, 0, 0, 0, 0, 0]
    ]
    game = Game(board)
    # other
    for column in range(0, 8):
        for row in range(0, 8):
            if not ((column == 3 or column == 4) and (row == 3 or row == 4)):
                assert game.get_pawn(Position(column, row)) is None, \
                    "value column: {}, row: {} should be empty".format(column, row)


def test_size_too_small():
    board = [[ 0 for _ in range(0,4)] for _ in range(0,4)]
    with pytest.raises(ValueError):
        Game(board)


def test_size_not_even():
    board = [[0 for _ in range(0, 11)] for _ in range(0, 11)]
    with pytest.raises(ValueError):
        Game(board)


def test_size_not_over_z_column():
    board = [[0 for _ in range(0, 27)] for _ in range(0, 27)]
    with pytest.raises(ValueError):
        Game(board)


def test_same_number_of_rows_and_column():
    board = [[0 for _ in range(0, 9)] for _ in range(0, 8)]
    with pytest.raises(ValueError):
        Game(board)


def test_initial_state_6():
    board = [
        [0, 0, 0, 0, 0, 0],
        [0, 0, 0, 0, 0, 0],
        [0, 0, 2, 1, 0, 0],
        [0, 0, 1, 2, 0, 0],
        [0, 0, 0, 0, 0, 0],
        [0, 0, 0, 0, 0, 0]
    ]
    game = Game(board)

    # d3
    assert game.get_pawn(Position(3, 2)).color == Color.BLACK
    # c4
    assert game.get_pawn(Position(2, 3)).color == Color.BLACK
    # c3
    assert game.get_pawn(Position(2, 2)).color == Color.WHITE
    # d4
    assert game.get_pawn(Position(3, 3)).color == Color.WHITE
    # other
    for column in range(0, 6):
        for row in range(0, 6):
            if not ((column == 2 or column == 3) and (row == 2 or row == 3)):
                assert game.get_pawn(Position(column, row)) is None


def test_cant_access_out_of_game():
    board = [
        [0, 0, 0, 0, 0, 0],
        [0, 0, 0, 0, 0, 0],
        [0, 0, 2, 1, 0, 0],
        [0, 0, 1, 2, 0, 0],
        [0, 0, 0, 0, 0, 0],
        [0, 0, 0, 0, 0, 0]
    ]
    game = Game(board)
    with pytest.raises(ValueError):
        game.get_pawn(Position(-1, -1))
    with pytest.raises(ValueError):
        game.get_pawn(Position(7, -1))
    with pytest.raises(ValueError):
        game.get_pawn(Position(7, 7))
    with pytest.raises(ValueError):
        game.get_pawn(Position(-1, 7))
