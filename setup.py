from setuptools import setup

setup(
    name='black_and_white',
    version='0.0.1',
    packages=['src'],
    url='',
    license='',
    author='Mickaël Sauvée',
    author_email='msauvee@tibco.com',
    description=''
)
